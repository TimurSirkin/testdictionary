#ifndef DICTIONARYCONTAINER_H
#define DICTIONARYCONTAINER_H

#include <iostream>
#include <vector>
#include "dictionary.h"
#include "notfoundexception.h"
using namespace std;

template<class TKey, class TValue>
class DictionaryContainer:Dictionary<TKey,TValue>
{

public:  
    // Dictionary interface
    const TValue& Get(const TKey& key) const
    {
        try
        {
            for (Node node : dictionaryVector)
            {
                if (&key == &(*node.key))
                {
                    return *node.value;
                }
            }
            throw DictionaryNotFoundException(key);
        }
        catch(DictionaryNotFoundException& err)
        {
            cout<<err.what()<<endl;
            // Some actions
        }
    }

    void Set(const TKey& key, const TValue& value)
    {
        if (!IsSet(key))
        {
            dictionaryVector.push_back(Node(key, value));
        }
    }

    bool IsSet(const TKey& key) const
    {
        for (Node node : dictionaryVector)
        {
            if (&key == &(*node.key))
            {
                return true;
            }
        }
        return false;
    }

    // Getter for size of Dictionary
    size_t Size() const
    {
        return dictionaryVector.size();
    }

 private:
    // Container for key and value
    struct Node
    {
        const TKey* key = nullptr;
        const TValue* value = nullptr;
        Node(){}
        Node(const TKey& initKey, const TValue& initValue)
        {
            key = &initKey;
            value = &initValue;
        }
    };

    // Main dictionary container
    vector<Node> dictionaryVector;

    // NotFoundException for DictionaryContainer
    class DictionaryNotFoundException:NotFoundException<TKey>
    {
    public:
        DictionaryNotFoundException(const TKey& errKey)noexcept
        {
             key = &errKey;
        }

        // NotFoundException interface
        const TKey& GetKey() const noexcept
        {
            return *key;
        }

        // Exception interface
        const char *what() const noexcept
        {
            return "Element not found in Dictionary";
        }

    private:
        const TKey *key;
    };
};

#endif
