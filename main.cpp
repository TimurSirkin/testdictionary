#include <iostream>
#include"dictionarycontainer.h"

using namespace std;

class TemplateKey
{
public:
    int value;
};

class TemplateValue
{
public:
    int value;
};

int main()
{
    DictionaryContainer<TemplateKey, TemplateValue> dictionary;

    TemplateKey k1;
    TemplateKey k2;
    TemplateKey k3;
    TemplateKey k4;
    TemplateValue v1;
    TemplateValue v2;
    TemplateValue v3;

    k1.value = 1;
    k2.value = 2;
    k3.value = 3;
    k4.value = 4;
    v1.value = 10;
    v2.value = 20;
    v3.value = 30;

    dictionary.Set(k1, v1);
    dictionary.Set(k2, v2);
    cout<< "Current size: " << dictionary.Size() << endl;

    // Trying to add existing key values
    dictionary.Set(k1, v2);
    dictionary.Set(k2, v1);
    cout<< "Current size: " << dictionary.Size() << endl;

    // Is this key setted?
    cout << "Is k3 setted? - ";
    if(dictionary.IsSet(k3))
    {
        cout << "true";
    }
    else
    {
        cout << "false";
    }
    cout << endl;

    dictionary.Set(k3, v3);
    cout<< "Current size: " << dictionary.Size() << endl;

    // Is this key setted?
    cout << "Is k3 setted? - ";
    if(dictionary.IsSet(k3))
    {
        cout << "true";
    }
    else
    {
        cout << "false";
    }
    cout << endl;

    // Trying to get value by existing key
    const TemplateValue &existV = dictionary.Get(k1);
    cout << "Value by getter = " << existV.value << endl;

    // Trying to to change value
    v1.value = 999;
    cout << "New value by getter = " << existV.value << endl;

    // Trying to get value by not existing key
    const TemplateValue &notExistV = dictionary.Get(k4);

    return 0;
}
