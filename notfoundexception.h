#ifndef NOTFOUNDEXCEPTION_H
#define NOTFOUNDEXCEPTION_H

template<class TKey>
class NotFoundException : public std::exception
{
public:
    virtual const TKey& GetKey() const noexcept = 0;
};

#endif // NOTFOUNDEXCEPTION_H
